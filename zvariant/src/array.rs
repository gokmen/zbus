use serde::ser::{Serialize, SerializeSeq, Serializer};

use crate::{Error, Result};
use crate::{IntoValue, Signature, Type, Value};

/// An unordered collection of items of the same type.
///
/// API is provided to create this from a [`Vec`].
///
/// [`Vec`]: https://doc.rust-lang.org/std/vec/struct.Vec.html
#[derive(Debug, Clone, PartialEq)]
pub struct Array<'a> {
    element_signature: Signature<'a>,
    elements: Vec<Value<'a>>,
}

impl<'a> Array<'a> {
    pub fn new<'s: 'a>(element_signature: Signature<'s>) -> Array<'a> {
        Array {
            element_signature,
            elements: vec![],
        }
    }

    pub fn append<'e: 'a>(&mut self, element: Value<'e>) -> Result<()> {
        if element.value_signature() != self.element_signature {
            return Err(Error::IncorrectType);
        }

        self.elements.push(element);

        Ok(())
    }

    pub fn get(&self) -> &[Value<'a>] {
        &self.elements
    }

    pub fn len(&self) -> usize {
        self.elements.len()
    }

    pub fn is_empty(&self) -> bool {
        self.elements.len() == 0
    }

    pub fn signature(&self) -> Signature<'static> {
        Signature::from_string_unchecked(format!("a{}", self.element_signature))
    }

    pub fn element_signature(&self) -> &Signature {
        &self.element_signature
    }
}

impl<'a> std::ops::Deref for Array<'a> {
    type Target = [Value<'a>];

    fn deref(&self) -> &Self::Target {
        self.get()
    }
}

impl<'a, T> From<Vec<T>> for Array<'a>
where
    T: Type + IntoValue<'a>,
{
    fn from(values: Vec<T>) -> Self {
        let element_signature = T::signature();
        let elements = values.into_iter().map(|value| value.into_value()).collect();

        Self {
            element_signature,
            elements,
        }
    }
}

impl<'a, T> From<&[T]> for Array<'a>
where
    T: Type + IntoValue<'a> + Clone,
{
    fn from(values: &[T]) -> Self {
        let element_signature = T::signature();
        let elements = values
            .iter()
            .map(|value| value.clone().into_value())
            .collect();

        Self {
            element_signature,
            elements,
        }
    }
}

impl<'a, T> From<&Vec<T>> for Array<'a>
where
    T: Type + IntoValue<'a> + Clone,
{
    fn from(values: &Vec<T>) -> Self {
        Self::from(&values[..])
    }
}

impl<'a> Serialize for Array<'a> {
    fn serialize<S>(&self, serializer: S) -> core::result::Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.elements.len()))?;
        for element in &self.elements {
            element.serialize_value_as_seq_element(&mut seq)?;
        }

        seq.end()
    }
}
